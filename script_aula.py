import time

start_time = time.time()

def fibonacci(x):
    if x == 0:
        return 0
    elif x == 1:
        return 1
    else:
        return fibonacci(x-2)+fibonacci(x-1)
print(fibonacci(37)) 
print(time.time()-start_time,'segundos')
